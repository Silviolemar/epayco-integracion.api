# epayco-integracion.api

[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

## Install

* Clona este repositorio `git clone https://gitlab.com/Silviolemar/epayco-integracion.api.git`
* Ejecuta `composer install`
* Crea una base de datos y una base de datos para pruebas en MySQL. Por ejemplo: `epayco`.
* Crear un archivo .env en base al arichivo .env.example con las credenciales para las base de datos.
* Ejecuta las migraciones y carga de datos iniciales y de prueba ejecutando  `php artisan migrate`
* Ejecuta las migraciones para la base de datos de pruebas `php artisan migrate:refresh`

## Contribute

¡Las contribuciones son siempre bienvenidas! Por favor, lea las pautas de [contribución] (https://gitlab.com/Silviolemar/epayco-integracion.api/blob/master/CONTRIBUTING.md) primero.

## Official Documentation

Documentación oficial [Lumen website](https://lumen.laravel.com/docs).

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).