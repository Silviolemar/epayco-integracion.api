<?php

namespace App\Helpers;

trait ArrayTrait
{
    static public function getArray($data)
    {
        $array = explode(',', $data);
        foreach ($array as $key => $value) {
            $array[$key] = trim($value);
        }
        return $array;
    }
}