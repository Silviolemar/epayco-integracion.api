<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\ArrayTrait;
use Epayco\Epayco;

class EpaycoController extends Controller
{
    use ArrayTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $epayco = new Epayco(array(
            "apiKey" => "75faf8321bb999f89f0998ce8a0c6f10",
            "privateKey" => "b6005acaf77a99c1a38d887b8bb91ecc",
            "lenguage" => "ES",
            "test" => true
        )); 

        $this->epayco = $epayco; 
   
    }

    public function createToken(Request $request)
    {     
        $token = $this->epayco->token->create($request->all());
        
        return response()->json($token);
    }
    
    public function createCustomers(Request $request)
    {     
        $customer = $this->epayco->customer->create($request->all());
        
        return response()->json($customer);
    }

    public function retrieveCustomer($id)
    {     
        $customer = $this->epayco->customer->get($id);
        
        return response()->json($customer);
    }

    public function customers()
    {     
        $customer = $this->epayco->customer->getList();
        
        return response()->json($customer);
    }
}