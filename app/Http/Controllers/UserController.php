<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Helpers\ArrayTrait;
//use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    use ArrayTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        
    }

    public function create(Request $request)
    {      
        return response($this->user->create($request->all()), 200);
    }

    public function getAll()
    {      
        return $this->user->all();
    }
}