<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'users'], function () use ($router) {
    $router->post('create', 'UserController@create');
    $router->get('getAll', 'UserController@getAll');
});

$router->group(['prefix' => 'epaycoInteg'], function () use ($router) {
    $router->post('createToken', 'EpaycoController@createToken');
    $router->group(['prefix' => 'customers'], function () use ($router) {
        $router->post('create', 'EpaycoController@createCustomers');
        $router->get('retrieve/{id}', 'EpaycoController@retrieveCustomer');
        $router->get('list', 'EpaycoController@customers');        
    });
});
